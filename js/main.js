$(document).ready(function(){
        $("#menu").on("click","a", function (event) {
            event.preventDefault();
            const id  = $(this).attr("href"),
            top = $(id).offset().top;
            $("body,html").animate({scrollTop: top}, 1500);
    });
});

const btn = $("#btn");  
$(window).scroll(function() {     
if ($(window).scrollTop() > $(window).height()) {
    btn.addClass("active");
    }else {
    btn.removeClass("active");
    }
});
btn.on("click", function(e) {
    e.preventDefault();
    $("html, body").animate({scrollTop:0}, 1500);
});

const bntClose = $("#close-btn")
const menu = $("#news")
bntClose.on("click",function(){
    $(menu).slideToggle(1500);  
});

